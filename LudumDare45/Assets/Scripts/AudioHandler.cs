﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] death = null;
    [SerializeField]
    private AudioClip[] select = null;
    [SerializeField]
    private AudioClip[] collide = null;
    [SerializeField]
    private AudioClip[] flourish = null;
    [SerializeField]
    private AudioClip[] attach = null;
    [SerializeField]
    private AudioClip[] explode = null;

    [SerializeField]
    private GraphHandler graphHandler;
    [SerializeField]
    private AudioSource creakingSource;

    private bool isCreaking;

    private System.Random random;

    private void Start()
    {
        this.random = new System.Random();
        this.isCreaking = true;

        graphHandler = graphHandler ?? GetComponent<GraphHandler>();
        if (!graphHandler)
            Debug.LogError("Missing graph handler. This script will not work.");

        creakingSource = creakingSource ?? GetComponent<AudioSource>();
        if (!creakingSource)
            Debug.LogError("Missing audio source to emit creaking sounds. These will not work.");
    }

    public AudioClip SampleSeedDeathSound()
    {
        return SampleItem(death);
    }

    public AudioClip SampleSeedExplodeSound()
    {
        return SampleItem(explode);
    }

    public AudioClip SampleSeedSelectSound()
    {
        return SampleItem(select);
    }

    public AudioClip SampleSeedCollideSound()
    {
        return SampleItem(collide);
    }

    public AudioClip SampleIslandFlourishSound()
    {
        return SampleItem(flourish);
    }

    public AudioClip SampleSeedAttachSound()
    {
        return SampleItem(attach);
    }

    /// <summary>
    /// Get a random item from a clip list.
    /// </summary>
    private T SampleItem<T>(T[] sources)
    {
        T result;
        if (sources != null && sources.Length > 0)
        {
            result = sources[random.Next(sources.Length)];
        }
        else
        {
            Debug.LogWarning("Cannot sample an item from a null or empty set.");
            result = default(T);
        }
        return result;
    }

    protected virtual void Update()
    {
        if (creakingSource != null)
        {
            if (graphHandler.HasEdge && (!isCreaking || !creakingSource.isPlaying))
            {
                isCreaking = true;
                // random time interval before creaking
                creakingSource.PlayDelayed((float)(random.NextDouble() * 15));
            }
            if (!graphHandler.HasEdge && isCreaking)
            {
                isCreaking = false;
                creakingSource.Stop();
            }
        }
    }
}
