﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class NodeIsland : MonoBehaviour
{
    private void Start()
    {
        this.UpdateClosest();
    }

    private void Update()
    {
        this.UpdateClosest();
    }

    private void UpdateClosest()
    {
        Node n = this.GetComponent<Node>();
        if (n == null)
            return;
        Island closest = null;
        float dist = float.MaxValue;
        foreach (Island island in GameObject.FindObjectsOfType<Island>())
        {
            float cur_dist = (island.transform.position - n.transform.position).magnitude;
            if (cur_dist < dist)
            {
                closest = island;
                dist = cur_dist;
            }
        }
        n.island = closest;
    }
}
