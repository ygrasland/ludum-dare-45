﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VirtualCameraChooser : MonoBehaviour
{
    [System.Serializable]
    public class IslandToCameraAssociation
    {
        public Island Island => island;
        [SerializeField]
        private Island island;

        public CinemachineVirtualCamera Camera => camera;
        [SerializeField]
        private CinemachineVirtualCamera camera;

        public IslandToCameraAssociation(Island island, CinemachineVirtualCamera camera)
        {
            this.island = island ?? throw new System.ArgumentNullException(nameof(island));
            this.camera = camera ?? throw new System.ArgumentNullException(nameof(camera));
        }

        public override string ToString()
        {
            return $"Island {island.name} (IsAlive: {island.IsAlive}) => Camera {camera.name}";
        }
    }

    public IEnumerable<IslandToCameraAssociation> IslandToCameraActivations => islandToCameraActivations;
    [SerializeField]
    private List<IslandToCameraAssociation> islandToCameraActivations;

    protected virtual void Start()
    {
        islandToCameraActivations = islandToCameraActivations ?? new List<IslandToCameraAssociation>();
        foreach (IslandToCameraAssociation i2c in islandToCameraActivations)
        {
            if (!i2c.Island || !i2c.Camera)
            {
                Debug.LogError($"Invalid association detected! This component is not properly conigured.");
                continue;
            }
            StartObservingAssociation(i2c);
        }

        // Activate the last valid camera
        var activableCamera = islandToCameraActivations.
            Where(i2c => i2c.Island.IsAlive).
            Select(i2c => i2c.Camera).
            FirstOrDefault();
        if (activableCamera)
        {
            ActivateCamera(activableCamera);
        }
    }

    /// <summary>
    /// Associates an island with a virtual camera, so that when this island becomes active the relevant camera is activated.
    /// Does not activate a new camera even if the island is active.
    /// </summary>
    public void AddIslandActivation(Island island, CinemachineVirtualCamera camera)
    {
        var newAssociation = new IslandToCameraAssociation(island, camera);
        islandToCameraActivations.Add(newAssociation);
        StartObservingAssociation(newAssociation);
    }

    /// <summary>
    /// Removes any camera association set for a given island.
    /// </summary>
    public void RemoveIslandActivation(Island island)
    {
        foreach (var i2c in islandToCameraActivations.Where(i2c => i2c.Island == island).ToList())
        {
            islandToCameraActivations.Remove(i2c);
            i2c.Island.StateChanged.RemoveListener(OnIslandActivationChanged);
        }
    }

    private void StartObservingAssociation(IslandToCameraAssociation association)
    {
        association.Island.StateChanged.AddListener(OnIslandActivationChanged);
    }

    void OnIslandActivationChanged(Island island, bool newState)
    {
        if (newState == true)
        {
            CinemachineVirtualCamera newCamera = islandToCameraActivations.
                FirstOrDefault(i2c => i2c.Island == island)?.Camera;
            if (newCamera)
            {
                ActivateCamera(newCamera);
            }
        }
    }

    private static void ActivateCamera(CinemachineVirtualCamera newCamera)
    {
        // The CinemachineBrain component choses the last activated virtual camera.
        var go = newCamera.gameObject;
        go.SetActive(false);
        go.SetActive(true);
        Debug.Log($"Activated virtual camera '{go.name}'");
    }
}
