﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandRenderings : MonoBehaviour
{
    public GameObject vegetation;

    SpriteRenderer spriteRenderer;


    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Florish()
    {
        SpriteRenderer[] renderers = vegetation.GetComponentsInChildren<SpriteRenderer>();
        foreach(var renderer in renderers)
        {
            renderer.sortingOrder = renderer.sortingOrder + spriteRenderer.sortingOrder;
        }
        vegetation.SetActive(true);
    }
}
