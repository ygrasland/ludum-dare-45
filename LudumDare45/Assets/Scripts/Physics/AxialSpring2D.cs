﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AxialSpring2D : MonoBehaviour {

    [Tooltip("Target angle of the spring")]
    public float targetRestAngle = 0;

    [Tooltip("How hard this spring will try to bring back the object to its rest angluar position")]
    public float stiffness = 1000;

    [Tooltip("Linear dampening")]
    public float dampening = 3;

    [Tooltip("Linear dampening")]
    public float max_torque = float.PositiveInfinity;

    [Tooltip("How hard this spring will try to bring back the object to its rest angluar position")]
    public Vector2 localFulcrum = Vector2.zero;

    private Rigidbody2D rb;

    protected void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Get the force application parameters to generate torque at the proper position
        Vector2 worldCenterOfMass = rb.worldCenterOfMass;
        Vector2 worldSpaceFulcrum = transform.TransformPoint(localFulcrum);
        Vector2 worldCOMToFC = worldSpaceFulcrum - worldCenterOfMass;
        float distanceToCOM = worldCOMToFC.magnitude;
        Vector2 forceDirection = Vector3.Cross(worldCOMToFC.normalized, new Vector3(0, 0, 1));

        // Compute how much Torque should be applied
        float deg_delta = targetRestAngle - transform.localEulerAngles.z;
        deg_delta = ((deg_delta + 540) % 360) - 180; // put it in [-180; +180]
        float rad_delta = Mathf.PI / 180 *(deg_delta);

        float appliedTorque = stiffness * rad_delta;
        appliedTorque -= dampening * rb.angularVelocity;
        appliedTorque = Mathf.Min(appliedTorque, max_torque);

        // Apply a force to the Center of Mass that will generate the propoer amount of torque
        Vector2 appliedForce = appliedTorque / distanceToCOM * forceDirection;
        rb.AddForce(appliedForce);
        // Debug.Log($"worldCenterOfMass = {worldCenterOfMass}, worldSpaceFulcrum = {worldSpaceFulcrum}, forceDirection = {forceDirection}, appliedTorque = {appliedTorque}, appliedForce = {appliedForce}");
    }
}
