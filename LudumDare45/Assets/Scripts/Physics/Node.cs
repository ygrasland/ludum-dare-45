﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Node : MonoBehaviour
{
    public enum StateEnum { Asleep, Ready, Pointed, Dragged, Active, Attached, Destroyed };

    private abstract class StateImpl
    {
        public virtual void Enter(Node node) { }
        public virtual void Update(Node node) { }
        public virtual void Leave(Node node) { }
    }

    public bool IsRooted
    {
        get { return isRooted; }
        set { isRooted = value; }
    }
    [SerializeField]
    private bool isRooted;

    public GraphHandler GraphHandler { get; set; }

    protected RigidbodyType2D? RigidbodyType
    {
        get { return rigidbodyType; }
        set
        {
            rigidbodyType = value;
            Rigidbody2D rigidbody = this.GetComponent<Rigidbody2D>();
            rigidbody.simulated = rigidbodyType.HasValue;
            if (rigidbodyType.HasValue)
                rigidbody.bodyType = rigidbodyType.Value;
        }
    }
    private RigidbodyType2D? rigidbodyType;

    public StateEnum CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            if (currentState != value)
                InstallState(value);
        }
    }

    [SerializeField]
    private StateEnum currentState = StateEnum.Asleep;
    private StateImpl currentStateImpl;

    protected bool IsPlayingSound => player.isPlaying;

    public List<Node> neighbours;
    public List<Edge> edges;
    public Island island;

    private Animator animator;
    private AudioSource player;
    private float? lastCollisionStart;

    [HideInInspector]
    public bool deathByFall = false;

    private static Dictionary<StateEnum, StateImpl> stateLogics;


    private void Start()
    {
        // initalise avec une rotation alatoire
        if (!IsRooted)
            this.transform.Rotate(Vector3.forward * (int)(this.GraphHandler.rand.NextDouble() * (180 - 0) + 0));
    }

    static Node()
    {
        stateLogics = new Dictionary<StateEnum, StateImpl>()
        {
            { StateEnum.Asleep, new AsleepStateImpl() },
            { StateEnum.Ready, new ReadyStateImpl() },
            { StateEnum.Pointed, new PointedStateImpl() },
            { StateEnum.Dragged, new DraggedStateImpl() },
            { StateEnum.Active, new ActiveStateImpl() },
            { StateEnum.Attached, new AttachedStateImpl() },
            { StateEnum.Destroyed, new DestroyedStateImpl() }
        };
    }

    public void MoveNode(Vector2 position)
    {
        transform.position = position;
    }

    protected virtual void OnEnable()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<AudioSource>();
        InstallState(currentState);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        bool willPlaySound = !lastCollisionStart.HasValue || (Time.time - lastCollisionStart.Value) > 0.3f;
        lastCollisionStart = Time.time;
        if (willPlaySound && !player.isPlaying)
            PlaySound(GraphHandler.AudioHandler.SampleSeedCollideSound());
    }

    protected virtual void Update()
    {
        // Self-destruct if this node has fallen in the shadow, before it encounters a Balrog
        if (this.transform.position.y < this.GraphHandler.lowBound && CurrentState != StateEnum.Destroyed)
        {
            deathByFall = true;
            this.GraphHandler.RemoveNode(this);
        }
    }

    protected virtual void OnDestroy()
    {
        RemoveEdges();
        foreach (Node n in this.neighbours)
            n.neighbours.Remove(this);
    }

    protected void PlaySound(AudioClip clip)
    {
        player.clip = clip;
        player.Play();
    }

    public void CreateEdges()
    {
        // find neighbours
        this.neighbours = new List<Node>();
        foreach (Node n in this.GraphHandler.graph)
        {
            if (n != this && (n.transform.position - this.transform.position).magnitude < this.GraphHandler.minDist
                && (n.IsRooted || n.CurrentState == StateEnum.Attached) && n.CurrentState != StateEnum.Asleep)
            {
                this.neighbours.Add(n);
                n.neighbours.Add(this);

                Edge e = Instantiate(this.GraphHandler.edgePrefab).GetComponent<Edge>();
                e.transform.parent = this.transform;
                e.gameObject.layer = LayerMask.NameToLayer("Branch");
                e.Init(this, n, true);
                this.edges.Add(e);
            }
        }
        if (this.edges.Count > 0)
        {
            this.CurrentState = StateEnum.Attached;
        }
    }

    public void RemoveEdges()
    {
        foreach (Edge edge in this.edges)
            Destroy(edge);
        // remove all edge to this node and clean neighbours
        foreach (Node n in this.neighbours)
        {
            foreach (Edge edge in n.edges)
            {
                if (edge.node2 == this || edge.node1 == this) // normalement que node2 de possible mais ça bug
                {
                    Destroy(edge);
                }
            }
        }
    }

    private void InstallState(StateEnum newState)
    {
        currentState = newState;
        currentStateImpl?.Leave(this);
        currentStateImpl = stateLogics[currentState];
        currentStateImpl.Enter(this);
    }

    private class AsleepStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("CanInteract", false);
            node.RigidbodyType = null;
        }
    }

    private class ReadyStateImpl: StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("Ready", true);
            node.animator.SetBool("CanInteract", false);
            node.RigidbodyType = RigidbodyType2D.Static;
        }
    }

    private class ActiveStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("Active", true);
            node.animator.SetBool("CanInteract", false);
            node.animator.SetBool("Grab", false);
            node.RigidbodyType = RigidbodyType2D.Dynamic;
        }
    }

    private class AttachedStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("Active", true);
            node.animator.SetBool("CanInteract", false);
            node.animator.SetBool("Grab", false);
            node.PlaySound(node.GraphHandler.AudioHandler.SampleSeedAttachSound());
            node.RigidbodyType = RigidbodyType2D.Dynamic;
        }
    }

    private class DraggedStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("Grab", true);
            node.animator.SetBool("CanInteract", false);
            node.PlaySound(node.GraphHandler.AudioHandler.SampleSeedSelectSound());
            node.RigidbodyType = null;
        }
    }

    private class PointedStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("CanInteract", true);
            node.animator.SetBool("Active", false);
        }
    }

    private class DestroyedStateImpl : StateImpl
    {
        public override void Enter(Node node)
        {
            base.Enter(node);
            node.animator.SetBool("Destroyed", true);
            if (node.deathByFall)
                node.PlaySound(node.GraphHandler.AudioHandler.SampleSeedDeathSound());
            else
                node.PlaySound(node.GraphHandler.AudioHandler.SampleSeedExplodeSound());
            node.RigidbodyType = null;
        }

        public override void Update(Node node)
        {
            // Destroy this node after it has finished playing its death sound
            base.Update(node);
            if (!node.IsPlayingSound)
                Destroy(node.gameObject);
        }
    }
}
