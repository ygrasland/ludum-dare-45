﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility
{
    public static LineRenderer createLineRendererObject(string name, float lineWidth, Vector3 initPos, Vector3 finalPos)
    {
        GameObject go = new GameObject(name);
        LineRenderer lr = go.AddComponent<LineRenderer>();
        lr.positionCount = 2;
        lr.startWidth = lineWidth;
        lr.endWidth = lineWidth;
        lr.SetPosition(0, initPos);
        lr.SetPosition(1, finalPos);
        return lr;
    }
}
