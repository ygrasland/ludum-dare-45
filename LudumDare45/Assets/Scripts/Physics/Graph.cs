﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    public float min_dist = 2.0f;

    public List<Node> nodes;

    public Graph()
    {
        this.nodes = new List<Node>();
    }
}
