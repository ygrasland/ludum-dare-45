﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
    public Node node1;
    public Node node2;
    public SpriteRenderer mySpriteRenderer;
    public EdgeCollider2D ec;
    public SpringJoint2D springJoint;
    public Transform tip;
    public Transform[] details;

    protected virtual void OnDestroy()
    {
        if (springJoint != null)
            Destroy(springJoint);
        Destroy(gameObject);
    }

    public void Init(Node node1, Node node2, bool enablePhysics)
    {
        this.mySpriteRenderer = GetComponent<SpriteRenderer>();
        this.ec = GetComponent<EdgeCollider2D>();
        this.node1 = node1;
        this.node2 = node2;

        // create spring
        if (enablePhysics)
        {
            springJoint = this.node1.gameObject.AddComponent<SpringJoint2D>();
            springJoint.connectedBody = this.node2.GetComponent<Rigidbody2D>();
            springJoint.dampingRatio = 10;
            springJoint.frequency = 10;

            if (details.Length > 0)
                details[Random.Range(0, details.Length)].gameObject.SetActive(true);
        }
        UpdatePosition();
    }

    public void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        transform.position = node1.transform.position;
        float spriteSize = tip.localPosition.x;
        float scale = (node1.transform.position - node2.transform.position).magnitude / spriteSize;
        transform.localScale = new Vector3(scale, 1, 1);
        Vector3 diff = (node2.transform.position - node1.transform.position).normalized;
        Quaternion q = Quaternion.FromToRotation(Vector3.right, diff);
        transform.rotation = q;

        //ec.points = new Vector2[2] { lr.GetPosition(0), lr.GetPosition(1) };
        ec.points = new Vector2[2] { transform.InverseTransformPoint(this.node1.transform.position),
                                     transform.InverseTransformPoint(this.node2.transform.position) };
    }
}
