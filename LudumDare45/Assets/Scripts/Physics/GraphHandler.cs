﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

[RequireComponent(typeof(AudioHandler))]
public class GraphHandler : MonoBehaviour
{
    public bool HasEdge
    {
        get
        {
            foreach (Node n in graph)
                if (n.edges.Count != 0)
                    return true;
            return false;
        }
    }

    public AudioHandler AudioHandler => audioHandler;
    private AudioHandler audioHandler;

    public GameObject nodePrefab;
    public Edge edgePrefab;

    public float minDist = 2.0f;
    public float lowBound = -10.0f;

    public GameObject endScreen;

    public List<Node> graph;

    public Island[] islands;

    private List<KeyValuePair<Node, Edge>> tempLines;

    private Node hoveredSeed;
    private Node.StateEnum hoveredSeedPreviousState;
    private Node grabbedSeed;

    private int seedsLayerIndex;

    public int islandCount;

    protected virtual void Awake()
    {
        audioHandler = GetComponent<AudioHandler>();
        seedsLayerIndex = LayerMask.NameToLayer("Seed");
    }

    public System.Random rand;

    protected virtual void Start()
    {
        rand = new System.Random();

        this.graph = new List<Node>();

        // get all islands
        this.islands = GameObject.FindObjectsOfType<Island>();
        foreach (Island island in this.islands)
            island.SetGraphHandler(this);

        // find all instance of node prefab already in scene and init them
        foreach (Node n in GameObject.FindObjectsOfType<Node>())
        {
            n.GraphHandler = this;
            this.graph.Add(n);
        }

        tempLines = new List<KeyValuePair<Node, Edge>>();
    }

    public void CreateNode(Vector2 position, Node.StateEnum state, bool findEdges)
    {
        // create gameobject from prefab
        GameObject obj = Instantiate(nodePrefab);
        obj.layer = seedsLayerIndex;
        obj.transform.position = position;
        Node n = obj.GetComponent<Node>();
        n.GraphHandler = this;
        n.CurrentState = state;
        if (findEdges)
            n.CreateEdges();
        this.graph.Add(n);
    }

    public void RemoveNode(Node obj)
    {
        Node n = this.graph.Find(x => x == obj);
        if (n != null)
        {
            this.graph.Remove(n);
            n.CurrentState = Node.StateEnum.Destroyed;
        }
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    public void EndLevel()
    {
        this.endScreen.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // test win condition
        if (islandCount >= GameObject.FindObjectsOfType<Island>().Length)
            this.EndLevel();

        // Cursor position
        Vector2 mouseWorldPos = GetWorldPositionOnPlane(Input.mousePosition, 0);

        // Get the current seed under the cursor.
        RaycastHit2D hit = Physics2D.Raycast(mouseWorldPos, Vector2.zero, Mathf.Infinity, 1 << seedsLayerIndex);
        Node hoverCandidate = hit.collider?.gameObject.GetComponent<Node>();

        // Since grabbed seeds are no longer simulated, we cannot raycast them and assume that they are still hovered.
        if (hoverCandidate == null && grabbedSeed != null)
            hoverCandidate = grabbedSeed;

        Node newHoveredSeed = null;
        if (hoverCandidate != null && !hoverCandidate.IsRooted && (
            hoverCandidate.CurrentState == Node.StateEnum.Ready ||
            hoverCandidate.CurrentState == Node.StateEnum.Active ||
            hoverCandidate.CurrentState == Node.StateEnum.Pointed ||
            hoverCandidate.CurrentState == Node.StateEnum.Dragged)
           )
            newHoveredSeed = hoverCandidate;

        // Produce visual feedback and state change for hovered seeds
        PerformSeedHovering(newHoveredSeed);

        // Perform the drag and drop
        PerformDragAndDrop(mouseWorldPos);

        // Debug functionality
        //PerformDebugOperations(mouseWorldPos);
    }

    private void PerformSeedHovering(Node newHoveredSeed)
    {
        bool startedHoveringOverNewSeed = newHoveredSeed != null && newHoveredSeed != hoveredSeed;

        // If another seed is no longer hovered, restore is pres-hovering state
        if (hoveredSeed != null && newHoveredSeed != hoveredSeed)
        {
            hoveredSeed.CurrentState = hoveredSeedPreviousState;
            hoveredSeed = null;
        }

        // Set the state of the hovered seed to get visual feedback
        if (startedHoveringOverNewSeed)
        {
            hoveredSeedPreviousState = newHoveredSeed.CurrentState;
            newHoveredSeed.CurrentState = Node.StateEnum.Pointed;
        }
        hoveredSeed = newHoveredSeed;
    }

    private void PerformDragAndDrop(Vector2 mouseWorldPos)
    {
        // Seeds that are indicated as future links when dragging
        List<Node> hintedSeeds = new List<Node>();

        if (Input.GetMouseButtonDown(0) && hoveredSeed != null)
        {
            // Start a drag and drop
            if (!hoveredSeed.IsRooted && (
                    hoveredSeed.CurrentState == Node.StateEnum.Ready ||
                    hoveredSeed.CurrentState == Node.StateEnum.Active ||
                    hoveredSeed.CurrentState == Node.StateEnum.Pointed
                ))
            {
                if (hoveredSeed.CurrentState == Node.StateEnum.Attached)
                    hoveredSeed.RemoveEdges();

                this.grabbedSeed = hoveredSeed;
                this.grabbedSeed.CurrentState = Node.StateEnum.Dragged;
            }
        }
        else if (Input.GetMouseButtonUp(0) && grabbedSeed != null)
        {
            // Complete a drag and drop
            this.grabbedSeed.CurrentState = Node.StateEnum.Active;
            this.grabbedSeed.CreateEdges();
            this.hoveredSeed = null;
            this.grabbedSeed = null;
        }
        else if (grabbedSeed != null)
        {
            // Continue dragging
            this.grabbedSeed.MoveNode(mouseWorldPos);

            // Make a list of nodes to which we should draw transparent branches
            hintedSeeds.AddRange(graph.
                Where(n => (
                    mouseWorldPos - (Vector2)n.transform.position).magnitude < minDist &&
                    (n.IsRooted || n.CurrentState == Node.StateEnum.Attached) &&
                    n.CurrentState != Node.StateEnum.Asleep));
        }

        // Delete temporary lines that have become superfluous
        for (int i = tempLines.Count - 1; i >= 0; i--)
            if (!hintedSeeds.Contains(tempLines[i].Key))
            {
                Edge branch = tempLines[i].Value;
                Destroy(branch);
                tempLines.RemoveAt(i);
            }

        // Add new branches for hinted nodes which do not have one yet
        foreach (Node node in hintedSeeds)
            if (!tempLines.Any(kvp => kvp.Key == node))
            {
                Edge newBranch = Instantiate(edgePrefab);
                newBranch.Init(node, grabbedSeed, false);
                Color col = newBranch.GetComponent<SpriteRenderer>().color;
                newBranch.GetComponent<SpriteRenderer>().color = new Color(col.r, col.g, col.b, 0.3f);

                tempLines.Add(new KeyValuePair<Node, Edge>(node, newBranch));
            }
    }

    private void PerformDebugOperations(Vector2 mouseWorldPos)
    {
        // Create a rooted seed
        if (Input.GetKeyDown(KeyCode.Space))
            this.CreateNode(mouseWorldPos, Node.StateEnum.Asleep, false);

        // Create an active seed
        if (Input.GetMouseButtonDown(2))
            this.CreateNode(mouseWorldPos, Node.StateEnum.Ready, false);

        // Remove a seed
        if (Input.GetMouseButtonDown(1) && hoveredSeed != null)
            this.RemoveNode(hoveredSeed);
    }
}
