﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringRenderer : MonoBehaviour
{
    public GameObject edgePrefab;

    //public float lineWidth = 0.01f;
    List<LineRenderer> renderers;

    // Start is called before the first frame update
    void Start()
    {
        this.renderers = new List<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (LineRenderer lr in renderers)
            Object.DestroyImmediate(lr.gameObject);
        renderers.Clear();

        foreach (SpringJoint2D sj in GetComponents<SpringJoint2D>())
        {
            GameObject attached = sj.connectedBody.gameObject;
            string name = "renderer_" + sj.attachedRigidbody.gameObject.name + "_" + attached.name;
            //LineRenderer lr = Utility.createLineRendererObject(name, this.lineWidth, Vector2.zero, Vector2.zero);
            LineRenderer lr = Instantiate(edgePrefab).GetComponent<LineRenderer>();
            lr.SetPosition(0, new Vector3(this.transform.position.x, this.transform.position.y, 0));
            lr.SetPosition(1, new Vector3(attached.transform.position.x, attached.transform.position.y, 0));
            this.renderers.Add(lr);
            EdgeCollider2D ec = lr.gameObject.GetComponent<EdgeCollider2D>();
            ec.points = new Vector2[2] { lr.GetPosition(0), lr.GetPosition(1) };
            Debug.Log(lr.GetPosition(0).ToString("F4"));
            Debug.Log(ec.points[0].ToString("F4"));
        }
    }
    void OnDestroy()
    {
        // virer les gameobjects avec les lignes
        foreach (LineRenderer lr in renderers)
            Object.Destroy(lr.gameObject);
        renderers.Clear();
    }
}
