﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActivatedChangedEvent : UnityEvent<Island, bool> { }

[RequireComponent(typeof(AudioSource))]
public class Island : MonoBehaviour
{

    /// <summary>
    /// This event is fired when the island state changes
    /// </summary>
    public ActivatedChangedEvent StateChanged
    {
        get { return stateChanged; }
    }
    [SerializeField]
    private ActivatedChangedEvent stateChanged;

    /// <summary>
    /// State of this island (alive or not)
    /// </summary>
    public bool IsAlive
    {
        get { return isAlive; }
        protected set
        {
            if (isAlive != value)
            {
                isAlive = value;
                OnIsActiveChanged();
            }
        }
    }
    [SerializeField]
    private bool isAlive = false;

    // if final island green, win
    public bool isFinal;

    private GraphHandler graphHandler;

    protected virtual void Start()
    {
        if (stateChanged == null)
            stateChanged = new ActivatedChangedEvent();
        StartCoroutine(WaitBeforeStart(2));
        //ApplyActivationState();
    }

    IEnumerator WaitBeforeStart(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        ApplyActivationState();
    }

    // island has been touched by the grace of nature and become all flourished
    protected virtual void OnIsActiveChanged()
    {
        ApplyActivationState();
        if (isAlive)
        {
            AudioSource player = GetComponent<AudioSource>();
            player.clip = graphHandler.AudioHandler.SampleIslandFlourishSound();
            player.Play();
        }
        stateChanged.Invoke(this, isAlive);
    }

    // island has been touched by the grace of nature and become all flourished
    private void ApplyActivationState()
    {
        // change the sprite for good look
        SpriteRenderer renderer = this.GetComponent<SpriteRenderer>();
        if (renderer)
            renderer.color = isAlive ? Color.green : Color.white; // todo


        // convert all the related seeds to become ready
        if (isAlive)
        {
            foreach(IslandRenderings island in GetComponentsInChildren<IslandRenderings>())
                island.Florish();

            this.graphHandler.islandCount++;
            if (this.isFinal)
                this.graphHandler.EndLevel();

            foreach (Node n in graphHandler.graph)
                if (n.island == this && n.CurrentState == Node.StateEnum.Asleep)
                    n.CurrentState = Node.StateEnum.Ready;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Node n = collision.gameObject.GetComponent<Node>();
        if (n != null && !IsAlive && (n.IsRooted || n.CurrentState == Node.StateEnum.Attached))
        {
            IsAlive = true;
        }
    }

    public void SetGraphHandler(GraphHandler graphHandler)
    {
        this.graphHandler = graphHandler;
    }
}
