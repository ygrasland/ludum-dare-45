﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Obstacle : MonoBehaviour
{
    public GraphHandler GraphHandler;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Edge e = collision.collider.gameObject.GetComponent<Edge>();
        if (e != null)
        {
            Destroy(e);
            return;
        }
        
        Node n = collision.collider.gameObject.GetComponent<Node>();
        if (n != null)
        {
            GraphHandler.RemoveNode(n);
            Destroy(n);
        }
    }
}
