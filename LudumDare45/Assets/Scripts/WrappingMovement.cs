﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrappingMovement : MonoBehaviour
{
    public float speed = 10.0f;
    public Vector2 limits = new Vector2(-200, 200);

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        if (pos.x > limits.y)
            pos.x = limits.x;
        pos += Vector3.right * speed * Time.deltaTime;
        transform.position = pos;
    }
}
