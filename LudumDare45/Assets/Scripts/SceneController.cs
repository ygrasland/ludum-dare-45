﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public FadeController fadeController;
    public float transitionDelay = 1.0f;


    IEnumerator Load(string levelName)
    {
        fadeController.FadeOut();
        yield return new WaitForSeconds(transitionDelay);
        SceneManager.LoadScene(levelName);
    }


    IEnumerator Exit()
    {
        fadeController.FadeOut();
        yield return new WaitForSeconds(transitionDelay);
        Debug.Log("Game is exiting");
        Application.Quit(0);
    }

    public void LoadScene(string levelName)
    {
        StartCoroutine(Load(levelName));
    }

    public void ReloadScene()
    {
        StartCoroutine(Load(SceneManager.GetActiveScene().name));
    }

    public void ExitApplication()
    {
        StartCoroutine(Exit());
    }
}
